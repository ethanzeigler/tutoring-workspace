package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -  
//Class -
//Lab  -

public class BiggestDouble
{
	private double one,two,three,four;

	public BiggestDouble()
	{
		this(0,0,0,0);
	}

	public BiggestDouble(double a, double b, double c, double d)
	{
	    setDoubles(a,b,c,d);
	}

	public void setDoubles(double a, double b, double c, double d)
	{
	    one = a;
	    two = b;
	    three = c;
	    four = d;
	}

	public double getBiggest()
	{
	    if(one > two && one > three && one > four)
	    {
	        toString();
	        return one;
	    }
	    if(two > one && two > three && two > four)
	    {
	        toString();
	        return two;
	    }
	    if(three > one && three > two && three > four)
	    {
	       toString();
	       return three;
	    }
	    if(four > one && four > two && four > three)
	    {
	        toString();
	        return four;
	    }
	    return 0.0;
	}

	public String toString()
	{
	    System.out.println(one + " " + two + " " +  three + " " + four);
	    return "";
	}
}