package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -  
//Class -
//Lab  -

public class Triples
{
    private int number;

    public Triples()
    {
        this(0);
        // a squared + b squared = c squared
        //if a = odd, then b must = even, vice versa. c must be odd
        //GCF of a,b,c must be no greater than 1
    }

    public Triples(int num)
    {
        setNum(0);
    }

    public void setNum(int num)
    {
        number = num;
    }

    private int greatestCommonFactor(int one, int two, int three)
    {
        int max = 0;
        if (one == 0 || two == 0 || three == 0)
        {
            return 0;
        }
        int smaller = Math.max(one, Math.max(two,three));


        for (int i = smaller; i > 0; i--) {
            if(one % i == 0 && two % i == 0 && three % i == 0)
            {
                return i;
            }
        }
        return 1;
    }

    public String toString()
    {
        String output="";
        return output + "\n" ;
    }
}