package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -  
//Date -
//Class -
//Lab  -

import java.util.Scanner;

public class GuessRunner
{
	public static void main(String args[])
	{
		Scanner rdr = new Scanner(System.in);
		Guess guess;
	        char response;
	        
	        System.out.println("Guessing Game - how many numbers?");
	        int x = rdr.nextInt();
	        guess = new Guess(x);
	        guess.playGame();
	}
}