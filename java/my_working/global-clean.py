#!/usr/local/bin/python3

import glob
import os
import shutil
from os.path import curdir
from shutil import copy2
import subprocess

# traverse root directory, and list directories as dirs and files as files
for root, dirs, files in os.walk("."):
    for file in files:
        file_path = os.path.join(root, file)
        if os.path.isfile(file_path) and (
                file.endswith('.class') or
                file.endswith('.ctxt') or
                file.endswith('.bluej') or 
                file.endswith('.jcp')):
            os.remove(file_path)

for dir in os.listdir('.'):
    if not os.path.isdir(dir):
        continue
    os.chdir(f'{os.curdir}/{dir}/Labs')
    shutil.copy2('../../Makefile', '.')
    subprocess.run(['make', 'clean'])
    # if not os.path.exists('docs'):
    #     os.mkdir('docs') 
    #     source = '*.doc'
    #     dest = 'docs'
    #     to_move = glob.glob(source)
    #     for f in to_move:
    #         shutil.move(f, dest)
    #     os.chdir('docs')
    #     subprocess.run(['soffice', '--writer', '--convert-to', 'pdf', '*.doc'], shell=True, stdout=subprocess.PIPE)
    #     os.chdir('..')
        
    subprocess.run(["iconv", "-f", "utf-8", "-t", "utf-8", "-c", "*.java"], shell=True, stdout=subprocess.PIPE)
    subprocess.run(["javac", "*.java"], shell=True, stdout=subprocess.PIPE)
    os.chdir('../..')

