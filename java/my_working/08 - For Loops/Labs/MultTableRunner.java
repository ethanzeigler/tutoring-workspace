package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import java.util.Scanner;

public class MultTableRunner
{
	public static void main ( String[] args )
	{
		Scanner rdr = new Scanner(System.in);
		MultTable multi = new MultTable();
		for(int x = 0; x < 5; x++)
        {
			System.out.print("Enter a stop number and a number to mulitply: ");
			int number = rdr.nextInt();
			int stop = rdr.nextInt();
			multi.setTable(number, stop);
			multi.printTable();
		}
		
	}
}