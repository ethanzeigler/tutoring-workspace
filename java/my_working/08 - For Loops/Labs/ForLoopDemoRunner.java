package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class ForLoopDemoRunner
{
	public static void main ( String[] args )
	{
		ForLoopDemo.runForLoop(2,90,5);
		System.out.println("\n");
                ForLoopDemo.runForLoop(3,76,4);
		System.out.println("\n");
		ForLoopDemo.runForLoop(-10,8,2);
		System.out.println("\n");
		ForLoopDemo.runForLoop(5,30,2);
		System.out.println("\n");
		ForLoopDemo.runForLoop(100,150,5);
	}
}