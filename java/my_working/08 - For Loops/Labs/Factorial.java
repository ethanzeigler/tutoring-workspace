package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class Factorial
{
	private int number;

	public Factorial()
	{
	    
	}

	public Factorial(int num)
	{
	    number = 0;
	}

	public void setNum(int num)
	{
	    number = num;
	}

	public int getNum()
	{
		for(int x = number; x < 0; x--)
                {
                    number = number * x;
                }
	        return number;
	}

	public long getFactorial( )
	{
		long factorial = number;
                
                return factorial;
	}

	public String toString()
	{
		return "factorial of " + getNum() + " is "+ getFactorial()+"\n";
	}
}