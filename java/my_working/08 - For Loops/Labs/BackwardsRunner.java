package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class BackwardsRunner
{
	public static void main ( String[] args )
	{
		Backwards wrd = new Backwards();
		
		wrd.setString("Hello");
		System.out.println(wrd);
		
		wrd.setString("World");
		System.out.println(wrd);
		
		wrd.setString("JukeBox");
		System.out.println(wrd);
		
		wrd.setString("TCEA");
		System.out.println(wrd);
		
		wrd.setString("UIL");
		System.out.println(wrd);
	}
}