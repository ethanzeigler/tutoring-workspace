package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

/*
1. Create a scanner
2. Create a loop
3. Print message asking for input
	(This is nothing more than a message! It does nothing!)
4. Get the user input using the scanner
5. Take the user input and use it to create a new
	object or *put it into an existing one*. It doesn't
	teleport!
6. Output the object's result
*/

import java.util.Scanner;
public class GCDRunner
{
	public static void main ( String[] args )
	{
		 Scanner rdr = new Scanner(System.in);
		 for(int x = 0; x < 7; x++)
		 {
			GCD test = new GCD();
		        System.out.print("Enter the first number: ");
			int num1 = rdr.nextInt();
			System.out.print("Enter the second number: ");
			int num2 = rdr.nextInt();
			test.setNums(num1,num2);
			System.out.println(test);
                        
		 }		
	}
}