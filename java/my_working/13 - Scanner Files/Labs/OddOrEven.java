// A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

public class OddOrEven
{
    private int number;

    public OddOrEven()
    {
        setNum(0);
    }

    public OddOrEven(int num)
    {
        setNum(num);
    }

    public void setNum(int num)
    {
        number = num;
    }

    public boolean isOdd( )
    {
        if(number % 2 == 0)
        {
            return false;
        }

        return true;
    }

    public String toString()
    {

        if(isOdd() == true)
        {
            return number+ " is odd.\n\n";
        }
        else
        {
            return number+" is even.\n\n";
        }
    }
}