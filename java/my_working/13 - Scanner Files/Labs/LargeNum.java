//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

import java.util.Scanner;
public class LargeNum {
    private String line;

    public LargeNum() 
    {
        line = "";
    }

    public LargeNum(String s) {
        line = s;
    }

    public int getLargest()
    {
        Scanner scn = new Scanner(line);
        int largest = scn.nextInt();
        do
        {
            largest = Math.max(largest, scn.nextInt());   
        } while(scn.hasNext());

        return largest;
    }

    public String toString( )
    {
        return "- \t Largest == " + getLargest();
    }
}