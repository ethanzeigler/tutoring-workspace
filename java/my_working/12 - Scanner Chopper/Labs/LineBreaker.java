
//Name -
//Date -
//Class - 
//Lab  -

import java.util.Scanner;

public class LineBreaker
{
   private Scanner rdr;
   private String line;
   private int breaker;

   public LineBreaker()
   {
   	this("",0);
   }

   public LineBreaker(String s, int b)
   {
	   setLineBreaker(s, b);
   }

	public void setLineBreaker(String s, int b)
	{
		line = s;
		breaker = b;
		resetScanner();
	}

	private void resetScanner() {
		rdr = new Scanner(line);
	}

	public String getLine()
	{
		return line;
	}

	public String getLineBreaker()
	{
		String box ="";

		while(rdr.hasNext()){
			for (int i = 0; i < breaker && rdr.hasNext(); i++) {
				box = box + rdr.next();
			}
			box = box + "\n";
	    }
		resetScanner();
		return box;
	}

	public String toString()
	{
		return line + "\n" + getLineBreaker();
	}
}