

import java.util.Scanner;
import java.lang.Math;

public class DogFood
{
   private double amount;

   public DogFood()
   {	
      this("");	
   }

   public DogFood(String line)
   {
		Scanner chopper = new Scanner(line);
		double cups = 0;

      while(chopper.hasNext())
      {
         int weight = chopper.nextInt();
         cups += needsToEat(weight) * 7;
      }
        amount = cups;
   }

   public double getAmount()
   {
      return amount / 60;
   }

	public String toString()
	{
		return "" + (int) Math.ceil(getAmount()) + " - 10  POUND BAGS";
	}
   private double needsToEat(int weight) {
      if (weight < 5)
      {
         return 0.5;
      }
      else if (weight < 8)
      {
         return 1.0;
      }
      else if (weight < 10)
      {
         return 1.5;
      }
      else if (weight < 20)
      {
         return 2.0;
      }
      else if (weight < 40)
      {
         return 3.5;
      }
      else if (weight < 60)
      {
         return 4.5;
      }
      else if (weight < 80)
      {
         return 6.0;
      }
      else 
      {
         return 7.5;
      }
   }
}
//Daily Dog Food Usage

// Weight(pounds)	Food
// 2-4	   0.5
// 5-7	   1.0
// 8-9	   1.5
// 10-19	   2.0
// 20-39	   3.5
// 40-59	   4.5
// 60-79	   6.0
// >=80		7.5
