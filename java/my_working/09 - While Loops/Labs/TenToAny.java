package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class TenToAny
{
    private int base10;
    private int newBase;

    public void setNewNum(int bTen,int newB)
    {
        base10 = bTen;
        newBase = newB;
    }

    public String getNewNum()
    {
        String newNum = "";
        int newBaseTen = base10;
        while (newBaseTen > 0)
        {
            newNum = "" + newBaseTen % newBase + newNum;
            newBaseTen = newBaseTen / newBase;
        }
        System.out.println(base10 + " base 10 is " + newNum + " is base " + newBase);
        return newNum;
    }

    public String toString()
    {
        return "" + getNewNum();
    }

}