package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class ReverseNumberRunner
{
	public static void main( String args[] )
	{
		ReverseNumber digi = new ReverseNumber();
		
		digi.setNumber(234);
		digi.toString();
		
		digi.setNumber(111);
		digi.toString();
		
		digi.setNumber(9005);
		digi.toString();
		
		digi.setNumber(84645);
		digi.toString();
		
		digi.setNumber(8547);
		digi.toString();
		
		digi.setNumber(123456789);
		digi.toString();
	}
}