package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class LetterRemover
{
    private String sentence;
    private char lookFor;

    public LetterRemover()
    {
        setRemover("", '0');
    }
    //    public LetterRemover(String sentence, char lookFor)
    //    {
    //        
    //    }
    public void setRemover(String s, char rem)
    {
        sentence = s;
        lookFor = rem;
    }

    public String removeLetters()
    {
        return sentence.replaceAll(lookFor + "", "");
    }

    public String toString()
    {
        return sentence + " - letter to remove " + lookFor + "\n"
        + removeLetters();

    }
}