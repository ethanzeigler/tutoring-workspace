package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class LetterRemoverRunner
{
	public static void main( String args[] )
	{
		LetterRemover wrd = new LetterRemover();
		
		wrd.setRemover("I am Sam I am", 'a');
		System.out.println(wrd);
		System.out.println();
		
		wrd.setRemover("ssssssssxssssesssssesss", 'a');
		System.out.println(wrd);
		System.out.println();
					
		wrd.setRemover("qwertyqwertyqwerty", 'a');
		System.out.println(wrd);
		System.out.println();
		
		wrd.setRemover("abababababa", 'b');
		System.out.println(wrd);
		System.out.println();
		
		wrd.setRemover("abaababababa", 'x');
		System.out.println(wrd);
		System.out.println();
	}
}