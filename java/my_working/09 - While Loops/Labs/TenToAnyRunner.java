package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class TenToAnyRunner
{
	public static void main( String args[] )
	{
		TenToAny digi = new TenToAny();	
		
		digi.setNewNum(234, 9);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(100, 2);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(10, 2);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(15, 2);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(256, 2);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(100, 8);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(250, 16);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(56, 11);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(89, 5);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(23, 3);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(50, 5);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(55, 6);
		System.out.println(digi);
		System.out.println();
		
		digi.setNewNum(2500, 6);
		System.out.println(digi);
		System.out.println();
	}
}