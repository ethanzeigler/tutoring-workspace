package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class Perfect
{
    private int number;

    public void setNumber(int num)
    {
        number = num;
    }

    public boolean isPerfect()
    {
        int num = number - 1;
        int sum = 0;

        while(num > 0)
        {
            if(number % num == 0)
            {
                sum = sum + num;
            } 
            num--;
        }
        if(sum == number)
        {
            return true;
        }
        return false;
    }

    public String toString()
    {
        if(isPerfect())
        {
            return number + " is perfect";
        }
        else
        {
            return number + " is not perfect";
        }

    }

}