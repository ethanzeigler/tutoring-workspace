package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class DigitAvgRunner
{
	public static void main( String args[] )
	{
		DigitAvg digi = new DigitAvg();
		
		digi.averageDigits(234);
		digi.averageDigits(10000);
		digi.averageDigits(111);
		digi.averageDigits(9005);
		digi.averageDigits(84645);
		digi.averageDigits(8547);
		digi.averageDigits(123456789);
	}
}