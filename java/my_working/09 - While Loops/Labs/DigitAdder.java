package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class DigitAdder
{
    public static int sumDigits( int number )
    {
        int sum = 0;
        int x = number;
        while(x > 0)
        {
            sum = x % 10;
            sum = sum + x;
            x = x / 10;
        }
        System.out.println(sum + " is the sum of the digits of " + number);
        return sum;
    }
}