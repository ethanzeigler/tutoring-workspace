package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class DivisorsRunner
{
	public static void main(String args[])
	{
		Divisors digi = new Divisors();
		
		digi.getDivisors(10);
		digi.getDivisors(45);
		digi.getDivisors(14);
		digi.getDivisors(1024);
		digi.getDivisors(1245);
		digi.getDivisors(33);
		digi.getDivisors(65535);
	}
}