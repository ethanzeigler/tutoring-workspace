//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*;

public class WordsCompareRunner
{
    public static void main( String args[] )
    {
       WordsCompare test = new WordsCompare("abe","ape");
       test.compare();
       test.toString();
       out.println();
       
       test.setWords("giraffe","gorilla");
       test.compare();
       test.toString();
       out.println();
       
       test.setWords("one","two");
       test.compare();
       test.toString();
       out.println();
       
       test.setWords("fun","funny");
       test.compare();
       test.toString();
       out.println();
       
       test.setWords("123","19");
       test.compare();
       test.toString();
       out.println();
       
       test.setWords("193","1910");
       test.compare();
       test.toString();
       out.println();
       
       test.setWords("goofy","godfather");
       test.compare();
       test.toString();
       out.println();
       
       test.setWords("funnel","fun");
       test.compare();
       test.toString();
       out.println();
    }
}