package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  - 

import static java.lang.System.*;
import java.util.Scanner;

public class StringEqualityRunner
{
	public static void main( String args[] )
	{
	    Scanner rdr = new Scanner(System.in);
	    StringEquality test = new StringEquality();
	    for(int x = 0; x < 8; x++)
	    {
	        System.out.println("Enter your Strings : ");
	        String one = rdr.nextLine();
	        String two = rdr.nextLine();
	        test.setWords(one, two);
	        test.checkEquality();
	        System.out.print(test);
	    }
	}
}