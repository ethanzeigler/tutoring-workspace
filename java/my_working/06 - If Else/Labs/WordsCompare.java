//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*;

public class WordsCompare
{
    private String wordOne, wordTwo;
    private int compare;

    public WordsCompare()
    {
        setWords("","");
    }

    public WordsCompare(String one, String two)
    {
        setWords(one,two);
    }

    public void setWords(String one, String two)
    {
        wordOne = one;
        wordTwo = two;
    }

    public void compare()
    {
        compare = wordOne.compareTo(wordTwo);
        
        //compare = wordTwo.compareTo(wordOne);
    }

    public String toString()
    {
        if(compare<0)
        {
            System.out.println(wordOne + " should be placed before " + wordTwo + "\n");
            return "";
        }
        else
        {
            System.out.println(wordOne + " should be placed after " + wordTwo + "\n");
            return "";
        }
    }
}