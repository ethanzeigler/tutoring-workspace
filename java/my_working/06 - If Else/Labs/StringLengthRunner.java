package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*;
import java.util.Scanner;

public class StringLengthRunner
{
	public static void main( String args[] )
	{
	    Scanner stringLength = new Scanner(System.in);
	    StringLengthCheck test = new StringLengthCheck();
	    for(int x = 0; x < 8; x++)
	    {
	        System.out.println("Enter strings : ");
	        String one = stringLength.nextLine();
	        String two = stringLength.nextLine();
	        test.setWords(one,two);
	        test.checkLength();
	        System.out.print(test);
	    }
	}
}