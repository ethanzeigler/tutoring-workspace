package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  - 

import static java.lang.System.*;

public class Social
{
    private String socialNum;
    private int sum;

    public Social()
    {
        setWord(" ");
    }

    public Social(String soc)
    {
       setWord(soc);
    }

    public void setWord(String w)
    {
       socialNum = w;
    }

    public void chopAndAdd()
    {
        String[] nums = socialNum.split("-");
        sum = 0;
        for (int x = 0; x < nums.length; x++)
        {
           try
           {
              sum += Integer.parseInt(nums[x]); 
           }
           catch(Exception e)
           {
               
           }
        }
    }

    public String toString()
    {
        return "SS# " + socialNum + " has a total of " + sum + "\n";
    }
}