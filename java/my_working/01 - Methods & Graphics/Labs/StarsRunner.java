package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*;

public class StarsRunner
{
   public static void main(String args[])
   {
      StarsAndStripes test = new StarsAndStripes();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwoBlankLines();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwentyDashes();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwentyDashes();
      test.printTwentyStars();
      test.printTwentyDashes();
   }
}