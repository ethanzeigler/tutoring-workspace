package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

public class Triples
{
    private int number;

    public Triples()
    {
        this(1);
    }

    public Triples(int num)
    {
        number = num;
    }

    public void setNum(int num)
    {
        number = num;
    }
    
    public static String findTriples(int x)
    {	String total = "";
        int max = x;
        for(int a = 1; a <= max;a++)
        {
            for(int b = a+1; b <= max;b++)
            {
                for(int c = b+1; c <= max;c++)
                {
                    if((a*a)+(b*b)== (c*c))
                    {
                        if((a % 2 == 1 && b % 2 == 0 && c % 2 == 1) || (a % 2 == 0 && b % 2 == 1 && c % 2 == 1))
                        {
                           total += "\n" + a + " " + b + " " + " " + c; 
                        }  
                    }
                }
            }
        }
        return total;
    }

    public String toString()
    {
        String output="";
        return output + findTriples(number);
    }
}