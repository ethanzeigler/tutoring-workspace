package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;
import java.awt.Canvas;

class ColoredBoxes extends Canvas
{
    public ColoredBoxes()
    {
        setBackground(Color.BLACK);
    }

    public void paint( Graphics window )
    {
        window.setColor(Color.RED);
        window.setFont(new Font("TAHOMA",Font.BOLD,12));
        window.drawString("Graphics Lab Lab11g ", 20, 40 );
        window.drawString("Drawing boxes with nested loops ", 20, 80 );

        drawBoxes(window);
    }

    public void drawBoxes(Graphics window)
    {
        for(int row = 50; row<400;row+=15){
            for(int col = 100; col<400; col+=14){
                window.fillRect(row,  col,  10,  10);
                window.setColor(new Color(((int)(Math.random()*256)),((int)(Math.random()*256)),((int)(Math.random()*256))));
            }
        }
    }
}