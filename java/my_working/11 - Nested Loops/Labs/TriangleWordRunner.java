package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

public class TriangleWordRunner
{
   public static void main(String args[])
   {	
       TriangleWord triangle = new TriangleWord();
       
       triangle.setWord("FUN");
       System.out.print(triangle);
       
       triangle.setWord("COMPUTER");
       System.out.print(triangle);
       
       triangle.setWord("A");
       System.out.print(triangle);
       
       triangle.setWord("IT");
       System.out.print(triangle);
       
       triangle.setWord("TOAD");
       System.out.print(triangle);
   }
}