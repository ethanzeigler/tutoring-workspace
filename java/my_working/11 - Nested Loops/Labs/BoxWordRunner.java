package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

public class BoxWordRunner
{
   public static void main( String args[] )
   {
       BoxWord box = new BoxWord();
       
       box.setWord("SQUARE");
       System.out.println(box);
       
       box.setWord("BOX");
       System.out.println(box);
       
       box.setWord("A");
       System.out.println(box);
       
       box.setWord("IT");
       System.out.println(box);
       
       box.setWord("TOAD");
       System.out.println(box);
   }
}