package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

class BoxWord
{
    private String word;

    public BoxWord()
    {
        word = "";
    }

    public BoxWord(String s)
    {
        word = s; 
    }

    public void setWord(String w)
    {
        word = w;
    }

    public String toString()
    {
        String output = "";
        output += word;
        int count = word.length()-1;
        for(int i = 1; i < word.length()-1; i++){
            output+= "\n"+word.charAt(i) ;
            count--;
            for(int j = 0; j < word.length()-2; j++){
                output+= " ";
            }	
            output+=word.charAt(count);
        }
        StringBuffer sb = new StringBuffer(word);
        output += "\n"+ sb.reverse();
        return output+"\n";
    }
}