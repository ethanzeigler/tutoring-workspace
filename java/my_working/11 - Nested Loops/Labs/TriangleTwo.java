package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class TriangleTwo
{
    private int size;
    private String letter;

    public TriangleTwo()
    {
        setTriangle(0, "");
    }

    public TriangleTwo(String let, int sz)
    {
        setTriangle(sz, let);
    }

    public void setTriangle(int count, String let)
    {
        size = count;
        letter = let;
    }

    public String getLetter()
    {
        return "#";
    }

    public String toString()
    {
        String output = "";
        //return output;
        for (int i = size; i > 0; i--) {
            output += "\n";
            for (int c = i; c > 0; c--) 
                output += letter + "";

            return output+"\n";} 
        
        return output;
    }
}