package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

class TriangleWord
{
    private String word;

    public TriangleWord()
    {
        setWord("");
    }

    public TriangleWord(String w)
    {
        setWord(w);
    }

    public void setWord(String w)
    {
        word = w;
    }

    public String toString()
    {
        String tri = word;
        StringBuffer sb = new StringBuffer(tri);
        String output = "";
        int count = 0;
        int count2 = 1;
        if (tri.length() == 1)
            return tri;
        if (tri.length() % 2 == 1)
            for (int q = 0; q < (tri.length() / 2); q++) 
            {
                output += " ";
            }
        else
            for (int q = 0; q < (tri.length() / 2) - 1; q++)
                output += "  ";
        for (int p = 0; p < tri.length(); p++) 
        {
            for (int i = 0; i < (tri.length() / 2) - 1; i++) 
            {
                output += "  ";

            }
            if (count < tri.length() - 1) 
            {
                output += " " + tri.charAt(count) + "\n";
                count++;
            }
            for (int k = 1; k < count; k++) 
            {
                output += "";
            }
            if (count2 < tri.length() - 1) 
            {
                output += " " + tri.charAt(count2);
                count2++;
            }
        }
        output += sb.reverse() + tri.substring(1);

        return output;
    }
}