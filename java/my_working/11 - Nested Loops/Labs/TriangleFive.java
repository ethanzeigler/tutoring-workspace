package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

public class TriangleFive
{
    private char letter;
    private int amount;

    public TriangleFive()
    {
        setLetter(' ');
        setAmount(0);
    }

    public TriangleFive(char c, int amt)
    {
        setLetter(c);
        setAmount(amt);
    }

    public void setLetter(char c)
    {
        letter = c;
    }

    public void setAmount(int amt)
    {
        amount = amt;
    }

    public String toString()
    {
        String output = "";
        for(int i = 0; i < amount; i++)
        {
            char x = letter;
            for(int j = 0; j < amount - i; j++)
            {
                for(int k = 0; k < amount - j; k++)
                {
                    System.out.print(x);
                    
                }
                System.out.print(" ");
                if(x == 'Z') 
                {
                    x = 'A';
                }
                else 
                {
                   x++; 
                }
            }
            System.out.println();
        }
        return output;
    }
}