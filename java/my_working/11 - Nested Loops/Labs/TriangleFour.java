package Labs;

 

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class TriangleFour
{
   private int size;
   private String letter;

	public TriangleFour()
	{
	    setTriangle("", 0);
	}

	public TriangleFour(int count, String let)
	{
	    setTriangle(let, count);
	}

	public void setTriangle( String let, int sz )
	{
	    letter = let; 
	    size = sz;
	}

	public String getLetter() 
	{
	    String output = "";
	    for(int i = size; i > 0; i--)
	    {
	       for(int j = 0; j < size - i; j++)
	       {
	           output += " ";
	       }
	       for (int j = i; j > 0; j--)
	       {
	           output += letter;
	       }
	       output += "\n";
	    }
	    return output;
	}

	public String toString()
	{
		String output = "";
		return output + getLetter();
	}
}