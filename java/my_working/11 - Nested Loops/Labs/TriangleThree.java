//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class TriangleThree
{
    private int size;
    private String letter;

    public TriangleThree()
    {
        setTriangle(" ", 0);
    }

    public TriangleThree(int count, String let)
    {
        setTriangle(let, count);
    }

    public void setTriangle( String let, int sz )
    {
        letter = let;
        size = sz;
    }

    public String getLetter() 
    {
        String output = "";
        //return output;
        int count = 1;
        for (int i = size; i > 0; i--) {
            output += "\n";
            for (int c = i; c > 0; c--)
                output +=  " ";
            for(int x = count; x>0; x--)
                output += letter;
            count++;
        }

        {
           return output+"\n";
        }
        
    }
   
    public String toString()
    {
        String output = getLetter();
        return output;
    }
}  