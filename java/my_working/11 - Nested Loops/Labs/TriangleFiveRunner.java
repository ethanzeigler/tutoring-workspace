package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class - 
//Lab  -

public class TriangleFiveRunner
{
   public static void main(String args[])
   {
	TriangleFive tri = new TriangleFive();
	
	tri.setLetter('C');
	tri.setAmount(4);
	System.out.println(tri);
	
	tri.setLetter('A');
	tri.setAmount(5);
	System.out.println(tri);
	
	tri.setLetter('B');
	tri.setAmount(7);
	System.out.println(tri);
	
	tri.setLetter('X');
	tri.setAmount(6);
	System.out.println(tri);
	
	tri.setLetter('Z');
	tri.setAmount(8);
	System.out.println(tri);
   }
}