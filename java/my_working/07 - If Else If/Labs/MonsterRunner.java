//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import java.util.Scanner;
import static java.lang.System.*; 

public class MonsterRunner
{
    public static void main( String args[] )
    {
        Scanner keyboard = new Scanner(System.in);
        Monster test = new Monster();
        Monster other = new Monster();
        for(int x = 0; x < 4; x++)
        {
            System.out.println("Enter 1st monster's name : ");
            String name1 = keyboard.next();

            System.out.println("Enter 1st monster's size : "); 
            int howBig1 = keyboard.nextInt(); 

            System.out.println("Enter 2nd monster's name : ");
            String name2 = keyboard.next();

            System.out.print("Enter 2nd monster's size : ");
            int howBig2 = keyboard.nextInt();

            test = new Monster(name1, howBig1);
            other = new Monster(name2, howBig2);

            System.out.println("Monster 1 - " + name1 + " " + howBig1);
            System.out.println("Monster 2 - " + name2 + " " + howBig2);

            if(test.isBigger(other))
            {
                System.out.println("Monster one is bigger that Monster two"); 
            }
            else if(test.isSmaller(other))
            {
                System.out.println("Monster one is smaller that Monster two");
            }
            else
            {
                System.out.println("Monster one is the same size as Monster two"); 
            }

            if(test.namesTheSame(other))
            {
                System.out.println("Monster one has the same name as Monster two");
            }
            else
            {
                System.out.println("Monster one does not have the same name as Monster two"); 
            }
        } 
    }
}