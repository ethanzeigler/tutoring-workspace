//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*; 
import java.util.Scanner;

public class HexToBinaryRunner
{
    public static void main( String args[] )
    {
        HexToBinary test = new HexToBinary();
        Scanner rdr = new Scanner(System.in);
        for(int x = 0; x < 7; x++)
        {
           System.out.println("Enter a letter : ");
           char b = rdr.next().charAt(0);
           test.setHex(b);
           System.out.println(test);
        }
    }
}