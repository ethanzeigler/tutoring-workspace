package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class BackwardsRunner
{
	public static void main ( String[] args )
	{
		Backwards wrd = new Backwards();
		
		setString("Hello");
		System.out.print(wrd);
		
		setString("World");
		System.out.print(wrd);
		
		setString("JukeBox");
		System.out.print(wrd);
		
		setString("TCEA");
		System.out.print(wrd);
		
		setString("UIL");
		System.out.print(wrd);
	}
}