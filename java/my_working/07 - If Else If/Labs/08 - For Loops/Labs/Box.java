package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

public class Box
{
    private String word;

    public Box()
    {
        setWord(" ");
    }

    public Box(String s)
    {
        setWord(s);
    }

    public void setWord(String s)
    {
        word = s;
    }

    public void print( )
    {
        for(int x = word.length() ; x > 0; x--)
        {
            System.out.println(word);
        }
    }
}