//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*; 
import java.util.Scanner;

public class DecoderRunner
{
	public static void main( String args[] )
	{
               Scanner rdr = new Scanner(System.in);
	       Decoder test  = new Decoder();
	       for(int x = 0 ; x < 16; x = x + 2)
	       {
	          System.out.print("Enter a character: ");
	          char let = rdr.next().charAt(0);
	          test.setLetter(let);
	          System.out.println(test);
	       }
	       
	}
}