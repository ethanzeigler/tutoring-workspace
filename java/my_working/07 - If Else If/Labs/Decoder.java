//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*; 

public class Decoder
{
	private char letter;

	public Decoder()
	{
            setLetter('0');
	}

	public Decoder(char let)
	{
           setLetter(let); 
	}

	public void setLetter(char let)
	{
           letter = let;
	}

	public char deCode()
	{
		char result = 0;
                if(letter >= 'A' && letter <= 'Z')
                {
                   result = Character.toLowerCase(letter);  
                }
                else if ( letter >= 'a' && letter <= 'z')
                {
                   result = Character.toUpperCase(letter); 
                }
                else if(letter >= '0' && letter <= '9')
                {
                   result =(char)(letter + 'A' - '0');
                }
                else  
                {
                    result = '#';
                }
                return result;
	}

	public String toString()
	{
		return letter + " decodes to " + deCode();
	}
}