//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*; 

public class Grade
{
    private int numGrade;

    public Grade()
    {
        setGrade(0);  
    }

    public Grade(int grade)
    {
        numGrade = grade;
    }

    public void setGrade(int grade)
    {
        numGrade = grade; 
    }

    public String getLetterGrade( )
    {
        String letGrade = "";
        if(numGrade >= 90)
        {
            System.out.print("");
            letGrade = "A";
            System.out.println(numGrade + " is a " + letGrade + "\n");
        }
        else if (numGrade >= 80 && numGrade <= 90)
        {
            letGrade = "B"; 
            System.out.println(numGrade + " is a " + letGrade + "\n");
        }
        else if (numGrade >= 75  && numGrade <= 80)
        {
            letGrade = "C";   
            System.out.println(numGrade + " is a " + letGrade + "\n");
        }
        else if (numGrade >= 70  && numGrade <= 75)
        {
            letGrade = "D";   
            System.out.println(numGrade + " is a " + letGrade + "\n");
        }
        else if (numGrade <= 70)
        {
            letGrade = "F";   
            System.out.println(numGrade + " is a " + letGrade + "\n");
        }
        return letGrade;

    }
    public String toString()
    {
        return numGrade + " is a " + getLetterGrade() + "\n";
    }
}