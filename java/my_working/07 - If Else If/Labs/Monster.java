//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*; 

public class Monster
{
	private String name;
	private int howBig;	

	public Monster()
	{
            this("", 0);
	}

	public Monster(String n, int size)
	{
             name = n;
             howBig = size;
	}

	public String getName()
	{
		return name;
	}
	
	public int getHowBig()
	{
		return howBig;
	}
	
	public boolean isBigger(Monster other)
	{
            if(this.getHowBig() > other.getHowBig())
            {
	        return true;
	    }
            return false;
	}
	
	public boolean isSmaller(Monster other)
	{
	    if(!this.isBigger(other) && this.getHowBig() != other.getHowBig())   
	    {
	        return true; 
	    }
	    //call isBigger() use !
	    return false;
	}

        public boolean namesTheSame(Monster other)
	{
	    if(this.name.equals(other.name))
	    {
	      return true;  
	    }
	    return false;
	}
	
	public String toString()
	{
		return name + " " + howBig;
	}
}