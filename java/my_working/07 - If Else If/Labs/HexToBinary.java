//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import static java.lang.System.*; 

public class HexToBinary
{
    private char hex;

    public HexToBinary()
    {
       setHex('0');
    }

    public HexToBinary(char hexNum)
    {
       setHex(hexNum);
    }
    public void setHex(char hexNum)
    {
        hex = hexNum;
    }
    public String getBinary()
    {
        switch (Character.toUpperCase(hex))
        {
           case '0' : return "0000";
           case '1' : return "0001";
           case '2' : return "0010";
           case '3' : return "0011";
           case '4' : return "0100";
           case '5' : return "0101";
           case '6' : return "0110";
           case '7' : return "0111";
           case '8' : return "1000";
           case '9' : return "1001";
           case 'A' : return "1010";
           case 'B' : return "1011";
           case 'C' : return "1100";
           case 'D' : return "1101";
           case 'E' : return "1110";
           case 'F' : return "1111";
           default : return "ERROR";
        }
    }

    public String toString()
    {
        return String.format(" %s is %s in binary! ", hex, getBinary());
    }
}