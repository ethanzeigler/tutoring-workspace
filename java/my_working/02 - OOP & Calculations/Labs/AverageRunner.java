package Labs;

//� A+ Computer Science  -  www.apluscompsci.com
//Name - Jacob O'Toole
//Date - 8/22/19
//Class - 1x
//Lab  - 

public class AverageRunner
{
    public static void main( String[] args )
    {
        Average test = 	new Average();
        test.setNums(5,5);
        test.average();
        test.print();

        test.setNums(90,100);
        test.average();
        test.print();

        test.setNums(100,85.8);
        test.average();
        test.print();

        test.setNums(-100,55);
        test.average();
        test.print();

        test.setNums(15236,5642);
        test.average();
        test.print();

        test.setNums(1000,555);
        test.average();
        test.print();
    }
}