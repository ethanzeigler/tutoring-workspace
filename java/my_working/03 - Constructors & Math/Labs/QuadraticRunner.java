//� A+ Computer Science  -  www.apluscompsci.com
//Name -
//Date -
//Class -
//Lab  -

import java.util.Scanner; 
import static java.lang.System.*;
import static java.lang.Math.*;

public class QuadraticRunner
{
    public static void main( String[] args )
    {
        Scanner Quadratic = new Scanner(System.in);

        Quadratic demo = new Quadratic();
        System.out.println("Enter a :: ");
        int a = Quadratic.nextInt();
        
        System.out.println("Enter b :: ");
        int b = Quadratic.nextInt();
        
        System.out.println("Enter c :: ");
        int c = Quadratic.nextInt();
        
        demo.setEquation(a, b, c);
        demo.calcRoots();
        demo.print();
    }
}