//� A+ Computer Science
// www.apluscompsci.com

//if statement example using booleans

public class IfOneBoolean
{
    public static void main(String args[])
    {
        boolean isOdd=true;

        if(isOdd)
        {
            System.out.println("isodd");
        }
        //! = not
        if(!isOdd)
        {
           System.out.println("iseven");
        }

    }
}

