
public class Backwards
{
    private String word;

    public Backwards()
    {
        setString("");
    }

    public Backwards(String s)
    {
        setString(s);
    }

    public void setString(String s)
    {
        word = s;
    }

    public char getFirstChar()
    {
        return word.charAt(0);
    }

    public char getLastChar()
    {
        return word.charAt(word.length() - 1);

    }

    public String getBackWards()
    {
        return new StringBuilder(word).reverse().toString();
    }

    public String toString()
    {
        return getFirstChar() + "\n" + getLastChar() + "\n" + getBackWards() + "\n" + word;
    }
}