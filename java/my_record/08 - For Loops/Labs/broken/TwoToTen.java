
public class TwoToTen
{
    private String binary;

    public TwoToTen()
    {
        setTwo("");
    }

    public TwoToTen(String bin)
    {
        setTwo(bin);
    }

    public void setTwo(String bin)
    {
        binary = bin;
    }

    public long getBaseTen( )
    {
        long ten = 0;
        for(int x = binary.length()-1;x >= 0;x--)
        {
            if(binary.indexOf(x) == 1)
            {
                ten += (int) Math.pow(2,x);
            }
        }
        return ten;
    }

    public String toString()
    {
        return "" + binary + " == " + getBaseTen();
    }
}	
