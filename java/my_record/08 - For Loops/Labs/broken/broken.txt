Backwards: will not compile. See errors.txt
CoolNumbers: incomplete. Object not implemented properly.
DecreasingWord: incomplete. No user input.
Factorial: conditional of factorial computing loop incorrect.
LoopStats: incomplete. Not implemented at all.
TwoToTen: binary translation algorithm is wrong in several ways
    Test inputs do not match the ones given by the lab
VowelCounter: algorithm does not properly replace vowels. Replaces at random (check your index)
