
public class DecreasingWord
{
	private String word;

	public DecreasingWord()
	{
		word = "";
	}

	public DecreasingWord(String s)
	{
		setWord(s);
	}

	public void setWord(String s)
	{
		word = s;
	}

	public void print()
	{
		for(int end = word.length(); end >= 0; end--)
		{
		   System.out.println(word.substring(0, end));
		}
	}
}