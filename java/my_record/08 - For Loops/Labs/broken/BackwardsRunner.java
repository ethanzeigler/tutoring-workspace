

public class BackwardsRunner
{
	public static void main ( String[] args )
	{
		Backwards wrd = new Backwards();
		
		wrd.setString("Hello");
		System.out.println(wrd);
		System.out.println();
		
		wrd.setString("World");
		System.out.println(wrd);
		System.out.println();
		
		wrd.setString("JukeBox");
		System.out.println(wrd);
		System.out.println();
		
		wrd.setString("TCEA");
		System.out.println(wrd);
		System.out.println();
		
		wrd.setString("UIL");
		System.out.println(wrd);
		System.out.println();
	}
}