
public class VowelCounter {
    public static String getNumberString(String s) {
        int num = 0;
        String vowel = "";
        for (int x = 0; x < s.length(); x++) {
            char p = s.charAt(0);
            if (p == 'A' || p == 'a' || p == 'E' || p == 'e' || p == 'I' || p == 'i' || p == 'O' || p == 'o' || p == 'U'
                    || p == 'u') {
                p = ("" + num).charAt(0);
                num++;
                if (num == 10) {
                    num = 0;
                }
            }
            vowel = vowel + p;
        }
        return vowel;
    }
}
