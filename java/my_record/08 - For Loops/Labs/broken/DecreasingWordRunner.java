
import java.util.Scanner;

/*
1. Create a scanner
2. Create a loop
3. Print message asking for input
(This is nothing more than a message! It does nothing!)
4. Get the user input using the scanner
5. Take the user input and use it to create a new
object or *put it into an existing one*. It doesn't
teleport!
6. Output the object's result
 */

public class DecreasingWordRunner
{
    public static void main ( String[] args )
    {
       DecreasingWord obj = new DecreasingWord("Hello");
       obj.print();
    }
}
