
public class GCD
{
	private int one, two;

	public GCD()
	{
		setNums(0,0);
	}

	public GCD(int numOne, int numTwo)
	{
		setNums(one, two);
	}

	public void setNums(int numOne, int numTwo)
	{
		one = numOne;
		two = numTwo;
	}

	public long getGCD( )
	{
		if (one == 0 || two == 0)
		{
		    return 0;
		}
	        int smaller;

		if (one > two) 
		{
                    smaller = two;
		} 
		else 
		{
                    smaller = one;
		}

		for (int i = smaller; i > 0; i--) {
		   if(one % i == 0 && two % i == 0)
		   {
                     return (long) i;
		   }
		}
		return -1;
	}

	public String toString()
	{
              return "the gcd of " + one + " and " + two + " is " + getGCD();
	}
}