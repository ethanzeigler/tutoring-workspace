public class DigitCounter
{
   public static int countDigits( int number )
	{
		int sum = 0;
		int x = number;
		while(x > 0)
		{
		    x = x/10;
		    sum++;   
		}
		System.out.println(number + " contains " + sum + " digits ");
		System.out.println("");
		return sum; 
	}
}