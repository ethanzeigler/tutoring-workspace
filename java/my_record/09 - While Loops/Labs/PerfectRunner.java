public class PerfectRunner
{
	public static void main( String args[] )
	{
		Perfect digi = new Perfect();
		
		digi.setNumber(496);
		System.out.println(digi);
		
		digi.setNumber(49);
		System.out.println(digi);
		
		digi.setNumber(6);
		System.out.println(digi);
		
		digi.setNumber(14);
		System.out.println(digi);
		
		digi.setNumber(8128);
		System.out.println(digi);
		
		digi.setNumber(1245);
		System.out.println(digi);
		
		digi.setNumber(33);
		System.out.println(digi);
		
		digi.setNumber(28);
		System.out.println(digi);
		
		digi.setNumber(27);
		System.out.println(digi);
		
		digi.setNumber(33550336);
		System.out.println(digi);
	}
	
}