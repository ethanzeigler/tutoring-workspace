public class ReverseNumber
{
    private int number;

    public void setNumber(int num)
    {
       number = num;
    }

    public int getReverse()
    {
        int rev = 0;
        while(number > 0)
        {
            rev = rev * 10 + (number % 10);
            number = number / 10;
        }
        return rev;
    }

    public String toString()
    {
        System.out.println(number + " reversed is " + getReverse()); 
        System.out.println();
        return "" + getReverse();
    }

}