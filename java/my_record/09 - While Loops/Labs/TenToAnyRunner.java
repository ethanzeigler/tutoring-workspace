public class TenToAnyRunner
{
	public static void main( String args[] )
	{
		TenToAny digi = new TenToAny();	
		
		digi.setNewNum(234, 9);
		System.out.println(digi);
		
		digi.setNewNum(100, 2);
		System.out.println(digi);
		
		digi.setNewNum(10, 2);
		System.out.println(digi);
		
		digi.setNewNum(15, 2);
		System.out.println(digi);
		
		digi.setNewNum(256, 2);
		System.out.println(digi);
		
		digi.setNewNum(100, 8);
		System.out.println(digi);
		
		digi.setNewNum(250, 16);
		System.out.println(digi);
		
		digi.setNewNum(56, 11);
		System.out.println(digi);
		
		digi.setNewNum(89, 5);
		System.out.println(digi);
		
		digi.setNewNum(23, 3);
		System.out.println(digi);
		
		digi.setNewNum(50, 5);
		System.out.println(digi);
		
		digi.setNewNum(55, 6);
		System.out.println(digi);
		
		digi.setNewNum(2500, 6);
		System.out.println(digi);
	}
}