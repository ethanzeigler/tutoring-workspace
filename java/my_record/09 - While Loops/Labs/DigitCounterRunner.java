public class DigitCounterRunner
{
	public static void main( String args[] )
	{
	    DigitCounter digi = new DigitCounter();
	    
	    digi.countDigits(234);
	    digi.countDigits(10000);
	    digi.countDigits(111);
	    digi.countDigits(9005);
	    digi.countDigits(84645);
	    digi.countDigits(8547);
	    digi.countDigits(123456789);	
	}
}