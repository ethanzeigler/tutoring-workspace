public class DigitAvg
{
    private static int countDigits( int number )
    {
        int sum=0;
        while(number > 0)
        {
           number = number / 10; 
           sum = sum + 1;
        }
        return sum;
    }

    private static int sumDigits( int number )
    {
        int sum=0;
        while(number > 0)
        {
            sum = sum + (number % 10);
            number = number / 10;
        }
        return sum;
    }

    public static double averageDigits( int number )
    {
        double average = sumDigits(number) / countDigits(number);
        System.out.println(number + " has a digit average of " + average); 
        System.out.println();
        return 0.0; 
    }
}