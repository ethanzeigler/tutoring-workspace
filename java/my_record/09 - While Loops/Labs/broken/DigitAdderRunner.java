public class DigitAdderRunner
{
	public static void main( String args[] )
	{
		DigitAdder digi = new DigitAdder();
		
		digi.sumDigits(234);
		digi.sumDigits(10000);
		digi.sumDigits(111);
		digi.sumDigits(9005);
		digi.sumDigits(84645);
		digi.sumDigits(8547);
		digi.sumDigits(123456789);
	}
}