public class Divisors
{
    public static String getDivisors( int number )
    {
        String divisors="";
        int counter = 1;
        while(counter < number)
        {
            if(number % counter == 1)
            {
                divisors = divisors + " " + counter;
            }
            counter++;
        }
        System.out.print(number + " has divisors " + divisors);
        System.out.println();
        return divisors; 
    }
}