
public class BiggestStringRunner
{
	public static void main(String args[])
	{
		BiggestString run = new BiggestString("abc", "cba", "bca");
                System.out.println(run);
                System.out.println("biggest = " + run.getBiggest() + "\n");
                
                new BiggestString("one", "fourteen", "twenty");
                System.out.println("biggest = " + run.getBiggest() + "\n");
                
                new BiggestString("124323", "20009", "3434");
                System.out.println("biggest = " + run.getBiggest() + "\n");
                
                new BiggestString("abcde", "ABCDE", "1234234324");
                System.out.println("biggest = " + run.getBiggest() + "\n");
	}
}