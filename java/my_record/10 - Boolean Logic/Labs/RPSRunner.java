
import java.util.Scanner;

public class RPSRunner
{
    public static void main(String args[])
    {
        Scanner input = new Scanner(System.in);
        char response = 'y';
        String player = "";
         do{
        System.out.print("pick your weapon [R,P,S] :: ");
        player = input.next();
        
        RPS game = new RPS(player); 
        game.determineWinner();
        
        System.out.println("Would you like to play again, y or n?");
        response = input.next().charAt(0);
       }while(response == 'y');
    }
}



