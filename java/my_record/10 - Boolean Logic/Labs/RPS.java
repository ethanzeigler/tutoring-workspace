
import java.util.Scanner;

public class RPS
{
	private String playChoice;
	private String compChoice;

	public RPS()
	{
	    
	}

	public RPS(String player)
	{
	    setPlayers(player);
	}

	public void setPlayers(String player)
	{
	    playChoice = player;
	    int x = (int)(Math.floor(Math.random() * 2));
	    switch(x)
	    {
	        case 0: compChoice = "R"; break;
	        case 1: compChoice = "P"; break;
	        case 2: compChoice = "S"; break;
	    }
	    
	    System.out.println("Player had " + playChoice);
	    System.out.println("Computer had " + compChoice);
	}

	public String determineWinner()
	{
		String winner= "";
		if(playChoice.equals("R") && compChoice.equals("R"))
		{
		    System.out.println("!Draw Game");
		}
		if(playChoice.equals("R") && compChoice.equals("P"))
		{
		    System.out.println("!Computer wins <<Paper Covers Rock>>");
		}
		if(playChoice.equals("R") && compChoice.equals("S"))
		{
		    System.out.println("!Player wins <<Rock Breaks Scissors>>");
		}
		if(playChoice.equals("P") && compChoice.equals("R"))
		{
		    System.out.println("!Player wins <<Paper Covers Rock>>");
		}
		if(playChoice.equals("P") && compChoice.equals("P"))
		{
		    System.out.println("!Draw Game");
		}
		if(playChoice.equals("P") && compChoice.equals("S"))
		{
		    System.out.println("!Computer Wins <<Scissors Cuts Paper>>");
		}
		if(playChoice.equals("S") && compChoice.equals("R"))
		{
		    System.out.println("!Computer Wins <<Rock Breaks Scissors>>");
		}
		if(playChoice.equals("S") && compChoice.equals("P"))
		{
		    System.out.println("!Player Wins <<Scissors Cuts Paper>>");
		}
		if(playChoice.equals("S") && compChoice.equals("S"))
		{
		    System.out.println("!Draw Game");
		}
		return winner;
	}

	public String toString()
	{
		String output = "";
		return output;
	}
}